import './App.css';

import React from "react";
import {Component} from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

import About from "./routes/about";
import Account from "./routes/account";
import Receive from "./routes/receive";
import Send from "./routes/send";
import Tools from "./routes/tools";
import Doc from "./routes/doc";
import Add from "./routes/add";
import Landing from "./routes/add";
import Wallet from "./routes/add";


class App extends Component {
  constructor() {
    super();
    this.state = {
        name: "init",
        isThereAKeyInStorage:false,
        hasUserAddedAWallet:false,
        hasUserSigned:false,
        walletSelected: null
    }
  }

    _setState() {
      var list = JSON.parse(localStorage.getItem("key"));
      
      if (list[0].Active != null ) {
        this.state.isThereAKeyInStorage = true
        console.log(this.state.isThereAKeyInStorage)
      }
    }

  render() {
      return (
        <div className="App">

        <Router>
                  <div id='pageHeader'>
                    <div id="wrapperMain">
                      <ul className="mainTabs">
                        <li><Link style={{color: 'Azure', textDecoration: 'none'}} to="/about">About</Link></li>
                        <li><Link style={{color: 'Azure', textDecoration: 'none'}} to="/account">Account</Link></li>
                        <li><Link style={{color: 'Azure', textDecoration: 'none'}} to="/send">Send</Link></li>
                        <li><Link style={{color: 'Azure', textDecoration: 'none'}} to="/receive">Receive</Link></li>
                        <li><Link style={{color: 'Azure', textDecoration: 'none'}} to="/tools">Tools</Link></li>
                      </ul>
                  </div>

                <div id="wrapperRight">
                    <ul className="rightTabs">
                      <div></div>
                      <li><Link style={{color: 'Azure', textDecoration: 'none'}} to="/documentation">Doc</Link></li>
                      <li><Link style={{color: 'Azure', textDecoration: 'none'}} to="/add">Add</Link></li>
                    </ul>
                </div>
                </div>

                <div className="content">
                        <Route exact path="/About" component={About}/>  
                        <Route path="/Account" component={Account}/>
                        <Route path="/Send" component={Send}/>
                        <Route path="/Receive" component={Receive}/>
                        <Route path="/Tools" component={Tools}/>
                        <Route path="/Documentation" component={Doc}/>  
                        <Route path="/Add" component={Add}/>
                  </div>
          </Router>

        </div>
      );
    }
}

export default App;
