import React from "react";
import './add.css';

function refresh() {
    localStorage.setItem('key', '');
    console.log('refreshed');
}

function add() {

    function addAccount() {

        try {
            var list = JSON.parse(localStorage.getItem("key"));

            if (list = null) {
                var listAccounts = '';
                var listSecrets = '';
            } else {
                var listAccounts = list[0].Accounts;
                var listSecrets = list[0].Secrets;
            }


            var activeAccount = document.getElementById('awallet').value;
            var activeSecret = prompt("Enter secret");

            listAccounts.push(activeAccount);
            listSecrets.push(activeSecret);


            var newListAccounts = listAccounts;
            var newListSecrets = listSecrets;
            var newActive = [activeAccount, activeSecret]

            var temp = [{
                'Accounts': newListAccounts,
                'Secrets': newListSecrets,
                'Active': newActive
            }]

            localStorage.setItem('key', '');
            localStorage.setItem('key', JSON.stringify(temp));

            var account = JSON.parse(localStorage.getItem("key"));
            console.log(account);

            document.getElementById('awallet').value = '';


        } catch (e) {

            console.log("The key is unset. Setting a new one")

            var activeAccount = document.getElementById('awallet').value;
            var activeSecret = prompt("Enter secret");


            var newListAccounts = [activeAccount];
            var newListSecrets = [activeSecret];
            var newActive = [activeAccount, activeSecret]

            var temp = [{
                'Accounts': newListAccounts,
                'Secrets': newListSecrets,
                'Active': newActive
            }]

            localStorage.setItem('key', JSON.stringify(temp));

            var account = JSON.parse(localStorage.getItem("key"));
            console.log(account);

            document.getElementById('awallet').value = '';
        }
    }

    return ( 
        <div>
        <h2 > ADD </h2>

        <div class = "form">
            <form class = "form">
                <label class = "label" for = "awallet"> Add Address: </label>
                    <br></br>
                        <input class = "input"
                            type = "text"
                            id = "awallet"
                            name = "awallet"
                            placeholder = "ie. rf3rEJBSLk7vEvpgYuMUqu8wbHvRDrbJxP"> 
                        </input>
                    <br></br>
                <br></br> 
            </form> 
        
        <span class = "card_footer" >
            <button onClick = { addAccount }
                class = "send"
                type = "send"
                id = "send" > 
                < strong > Add </strong>
            </button >
            <button onClick = { refresh }
                class = "refresh"
                type = "refresh"
                id = "refresh" > < strong > Refresh </strong>
            </button >
        </span>
        </div>
        </div>
    );
}

export default add;