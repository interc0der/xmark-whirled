import React from "react";
import './tools.css';
const RippleAPI = require('ripple-lib').RippleAPI;
 
function tools() {


function checkBalance() {
  
  var address = document.getElementById('cwallet').value;

  var api = new RippleAPI({
    server: 'wss://s.altnet.rippletest.net:51233'
    });

  /*api.on('connected', () => {
      console.log('connected');
    });

  api.on('disconnected', (code) => {
      console.log('disconnected, code:', code);
      });*/

  api.connect().then(() => {  

      api.request("account_info", {
          account: address,
          strict: true,
          ledger_index: 'current',
          queue: true
      }).then((x) => {
          var xrp = x.account_data.Balance/1000000
          alert("This address holds "+ xrp + " XRP")
          return api.disconnect();
      });
  });
};

function reset() {
  document.getElementById('cwallet').value = '';
};

    return (
      <div>
        <h2>TOOLS</h2>
        <h2>Check Balance</h2>
        <div>
            <label class="label" for="cwallet">Check Balance:</label><br></br>
            <input class= "input" type="text" id="cwallet" name="cwallet" placeholder="ie. rf3rEJBSLk7vEvpgYuMUqu8wbHvRDrbJxP"></input><br></br>
            <span class = "card_footer">
            <button onClick={checkBalance} class="check" type="check" id="check"><strong>Submit</strong></button>
            <button onClick={reset} class="reset" type="reset" id="reset"><strong>Reset</strong></button>
            </span>
        </div>
      </div>
    );
  }
 
export default tools;