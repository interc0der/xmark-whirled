import React from "react";
import './send.css';
import placeholder from "../../logo/send_placeholder.svg"


const client = require("xrpl-client");  
const lib = require("xrpl-accountlib")

const api = new client.XrplClient("wss://s.altnet.rippletest.net:51233");

let currencyArray = [];

let accountDataTitle = [
    "Placeholder Image:",
    "Address:",
    "Username:",
    "Domain:",
    "Email Hash:",
    "Reserve:",
    "Next Sequence:",
    "MessageKey:"
  ];

  let accountData = {};


function validate() {

    api.on('close', (close) => {
        console.log('disconnected', close);
        });
    
    api.on('state', () => {
        console.log("The connection state has changed");
        });

    api.on('retry', (retry) => {
        console.log('New connection attempted', retry);
        });     
        
    api.on('reconnect', (reconnect) => {
        console.log('Reconnecting', reconnect);
        });        

try {
    var publickey = document.getElementById("rwallet").value;

    //api.connect();
    
    api.ready().then(() => {

      api.send({
        command: "account_info",
        account: publickey,
        strict: "true",
        ledger_index: "current",
        queue: "true"

    }).then((x) => {
        console.log("Valid address")
        console.log(x);

        document.getElementById('placeholder').src = "";
        document.getElementById('insertCommand').innerHTML= "";
        document.getElementById('Status').innerHTML = "";

        var temp;
        temp = document.createElement('span');
        temp.className = 'status';
        temp.innerHTML = "Valid address";
        temp.style= "color:green";
        document.getElementsByClassName('Status')[0].appendChild(temp);

        document.getElementById('Title').innerHTML = "Destination Account Information";


        document.getElementById('destinationImage').innerHTML = "";
        document.getElementById('destinationList').innerHTML = "";
        document.getElementById('acceptedList').innerHTML = "";

        currencyArray = [];

            accountData = [
              x.account_data.urlgravatar,
              x.account_data.Account,
              'None',
              x.account_data.Domain,
              x.account_data.EmailHash,
              'None',
              x.account_data.Sequence,
              x.account_data.MessageKey
            ];

            var j;
            var temp;

                for (j = 0; j < 1; j++) {

                  temp = document.createElement('img');
                  temp.className = 'image';
                  temp.src= accountData[j];
                  document.getElementsByClassName('destinationImage')[0].appendChild(temp);
                }

                for (j = 1; j < accountData.length-1; j++) {

                    temp = document.createElement('li');
                    temp.className = 'accountInfo';
                    temp.innerHTML = accountDataTitle[j] + ' ' + accountData[j];
                    document.getElementsByClassName('destinationList')[0].appendChild(temp);
                  }


                  for (j = 7; j < accountData.length; j++) {

                    temp = document.createElement('li');
                    temp.className = 'message';
                    temp.classid = 'message';
                    temp.innerHTML = accountDataTitle[j];
                    document.getElementsByClassName('destinationList')[0].appendChild(temp);

                    temp = document.createElement('div');
                    temp.innerHTML = accountData[j];
                    document.getElementsByClassName('message')[0].appendChild(temp);
                  }
              
            currencyArray.push({
              'issuer': '',
              'balance': x.account_data.Balance/1000000,
              'currency': 'XRP'
            });  

            //api.close();

            api.send({
                command:"account_lines",
                account: publickey,
                ledger_index: "validated"
  
            }).then((x) => {
  
              var i;
  
              for (i=0; i < x.lines.length; i++) {
                
                if (x.lines[i].currency == '534F4C4F00000000000000000000000000000000') {
                  x.lines[i].currency = 'SOLO'
                }
  
                currencyArray.push({
                  'issuer': x.lines[i].account,
                  'balance': x.lines[i].balance,
                  'currency': x.lines[i].currency
                });
                
              };
  
              var j;
              var temp;
  
                  for (j = 0; j < currencyArray.length; j++) {
                    temp = document.createElement('li');
                    temp.className = 'currency';
                    temp.innerHTML = currencyArray[j].currency + ' ' + currencyArray[j].balance ;
                    document.getElementsByClassName('acceptedList')[0].appendChild(temp);
                  }
              });


    }).catch((e) => {

        if (publickey=='') {
            document.getElementById('Status').innerHTML = "";
            document.getElementById('destinationImage').innerHTML = "";
            document.getElementById('destinationList').innerHTML = "";
            document.getElementById('acceptedList').innerHTML = "";
            document.getElementById('Title').innerHTML = "";
            document.getElementById('placeholder').src = placeholder;
            document.getElementById('insertCommand').innerHTML = "Add valid address to view destination information";

            //api.close();

        }else{
            console.log("Invalid address");

            document.getElementById('Status').innerHTML = "";
            document.getElementById('destinationImage').innerHTML = "";
            document.getElementById('destinationList').innerHTML = "";
            document.getElementById('acceptedList').innerHTML = "";
            document.getElementById('Title').innerHTML = "";
            document.getElementById('placeholder').src = placeholder;
            document.getElementById('insertCommand').innerHTML = "Add valid address to view destination information";
    
            var temp;
            temp = document.createElement('span');
            temp.className = 'status';
            temp.innerHTML = "Invalid address";
            temp.style= "color:red";
            document.getElementsByClassName('Status')[0].appendChild(temp);

            //api.close();
            }
        }) 
    }) 
    } catch(e) {
            console.log(e);
        }

    }

    function reset() {
        document.getElementById('rwallet').value = '';
        document.getElementById('amount').value = '';
        document.getElementById('Status').innerHTML = "";
        document.getElementById('destinationImage').innerHTML = "";
        document.getElementById('destinationList').innerHTML = "";
        document.getElementById('acceptedList').innerHTML = "";
        document.getElementById('Title').innerHTML = "";
        
        var list = JSON.parse(localStorage.getItem("key"));
        var account = list[0].Active[0];
        console.log(account);
      };

      function sendButton() {
    
        var destination = document.getElementById('rwallet').value;
        var amount = document.getElementById('amount').value;

        var tx;
        
        var list = JSON.parse(localStorage.getItem("key"));
        var secret= list[0].Active[1];

        const account = lib.derive.familySeed(secret);

        api.ready().then( async () => {
           
        const { account_data } = await api.send({
            command: "account_info",
            account: account.address,
        });


        console.log(
            `Account balance (XRP) ${(account_data.Balance)/1000000}`
        );

        // Wait until we know what the current ledger index is
        await api.ready();

        const LastLedgerSequence = api.getState().ledger.last + 2; // Expect finality in max. 2 ledgers

        const { id, signedTransaction } = lib.sign(
            {
            TransactionType: "Payment",
            Account: account.address,
            Destination: destination,
            Amount: String(amount*1000000),
            Sequence: account_data.Sequence,
            Fee: String(12),
            LastLedgerSequence,
            },
            account
        );

        console.log("Transaction hash:", id);

        api.send({ command: "subscribe", accounts: [account.address] });

        api
            .send({ command: "submit", tx_blob: signedTransaction })
            .then(({ accepted, engine_result }) =>
            console.log("Transaction sent:", accepted, engine_result)
            );

        api.on(
            "transaction",
            ({ transaction, meta, ledger_index, engine_result }) => {
            if (transaction.hash === id) {
                console.log(
                `Transaction in ledger:\n  ${ledger_index}\nTransaction status:\n  ${engine_result}\nDelivered amount:\n  ${meta.delivered_amount}`
                );
                if (typeof meta.delivered_amount === "string") {
                const xrp = meta.delivered_amount/1000000;
                console.log(`Delivered amount in XRP instead of drops:\n  ${xrp}`);
                }
                api.close();
            }
            }
        );

        api.on("ledger", ({ ledger_index }) => {
            if (ledger_index > LastLedgerSequence) {
            console.log(
                "Past last ledger & transaction not seen. Transaction failed"
            );
            api.close();
        }
    });
    })
};

        /*var api = new RippleAPI({
            server: 'wss://s.altnet.rippletest.net:51233'
            });

        api.on('connected', () => {
            console.log('connected');
          });

        api.on('disconnected', (code) => {
            console.log('disconnected, code:', code)
            });

        api.connect().then( async () => {  
            
            const preparedTx = await api.prepareTransaction({
            "TransactionType": "Payment",
            "Account": account,
            "Amount": api.xrpToDrops(amount),
            "Destination": destination
            }, {
            // Expire this transaction if it doesn't execute within ~5 minutes:
            "maxLedgerVersionOffset": 75
            })
    
            const maxLedgerVersion = preparedTx.instructions.maxLedgerVersion;
    
            console.log("Prepared transaction instructions:", preparedTx.txJSON)
            console.log("Transaction cost:", preparedTx.instructions.fee, "XRP")
            console.log("Transaction expires after ledger:", maxLedgerVersion)


            // Sign prepared instructions ------------------------------------------------
            const signed = api.sign(preparedTx.txJSON, secret)
            const txID = signed.id
            const tx_blob = signed.signedTransaction
            console.log("Identifying hash:", txID)
            console.log("Signed blob:", tx_blob)

            // Submit signed blob -------
            
            const earliestLedgerVersion = (await api.getLedgerVersion()) + 1
            const result = await api.submit(tx_blob)
            console.log("Tentative result code:", result.resultCode)
            console.log("Tentative result message:", result.resultMessage)
        


            // Wait for validation -------------------------------------------------------
            let has_final_status = false
            api.request("subscribe", {accounts: [destination]})
            api.connection.on("transaction", async (event) => {
            if (event.transaction.hash == txID) {
                console.log("Transaction has executed!", event)
                has_final_status = true

                // Check transaction results -------------------------------------------------
    
                tx = await api.getTransaction(txID);
                console.log(tx);
                console.log("Transaction result:", tx.outcome.result)
                console.log("Balance changes:", JSON.stringify(tx.outcome.balanceChanges))

                alert("SUCCESS! Transaction result: " + tx.outcome.result + "\nBalance changes: " + JSON.stringify(tx.outcome.balanceChanges))
                return api.disconnect();
                }
            })

            api.on('ledger', ledger => {
                if (ledger.ledgerVersion > maxLedgerVersion && !has_final_status) {
                    console.log("Ledger version", ledger.ledgerVersion, "was validated.")
                    console.log("If the transaction hasn't succeeded by now, it's expired")
                    has_final_status = true
                }
            })
                
        })
    }*/


function send() {


    return (
      <div>
            <div class = "gridwrapper">

                    <form className = "form">
                    <h2>Send Payment</h2>
                        <label class="label" for="rwallet">Destination Address:</label><br></br>
                            <input 
                                class= "input" 
                                type="text" 
                                id="rwallet" 
                                name="rwallet" 
                                placeholder="ie. rBJmekRzTZVPSW39RDwqMExHLXicdxnLgb" 
                                onChange={event => validate(event.target.value)}
                            ></input><br></br>
                        <span className="Status" id="Status"></span>
                        <br></br>
                        <label class="label" for="destination">Destination Tag:</label><br></br>
                            <input 
                                class= "input" 
                                type="text" 
                                id="tag" 
                                name="tag" 
                                placeholder="ie. 1222222"
                            ></input><br></br>
                        <br></br>
                        <label class="label" for="amount">Amount:</label><br></br>
                            <input 
                                class= "input" 
                                type="text" 
                                id="amount" 
                                name="amount" 
                                placeholder="ie. 10.05 XRP"
                            ></input>

                    <br></br> 
                    <br></br> 

                    <span class = "card_footer">
                        <button onClick={sendButton} class="send" type="send" id="send"><strong>Send Payment</strong></button>
                        <button onClick={reset} class="reset" type="reset" id="reset"><strong>Reset</strong></button>
                    </span>
                </form> 

                <div>
                    <h2 id = "Title"></h2>
                    
                    
                    <img id="placeholder" src={placeholder} width="200"></img>
                    <div id="insertCommand">Add valid address to view destination information</div>


                    <div className='destinationInfo'>
                        <div id='destinationImage' className='destinationImage'>
                        </div>
                        <ul id='destinationList' className='destinationList'>
                         </ul>
                    </div>

                    <div id='tokenList' className='tokenList'>
                        <ul id='acceptedList' className='acceptedList'>
                        </ul>
                    </div>

                </div>


            

            </div>
      </div>
    );
  }
 
export default send;