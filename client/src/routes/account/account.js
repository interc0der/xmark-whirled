import React from "react";
import './account.css';
import { useState, useEffect } from 'react';
import $ from 'jquery';

const client = require("xrpl-client");
const api = new client.XrplClient("wss://s.altnet.rippletest.net:51233");
//const lib = require("xrpl-accountlib");

//const RippleAPI = require('ripple-lib').RippleAPI;

let accountData = {
    Placeholder_Image: 'Pending...',
    Address: 'Pending...',
    Username: 'Pending...',
    Domain: 'Pending...',
    Email_Hash: 'Pending...',
    MessageKey: 'Pending...',
    Reserve: 'Pending...',
    Next_Sequence: 'Pending...'
};

let currencyArray = [];

let latestTxArray = [];

let accountDataTitle = [
    "Placeholder Image:",
    "Address:",
    "Username:",
    "Domain:",
    "Email Hash:",
    "Reserve:",
    "Next Sequence:",
    "MessageKey:"
]

function Account() {

    useEffect(() => {

        let accountRefresh = document.getElementById('accountList');
        accountRefresh.innerHTML = '';

        let gridRefresh = document.getElementById('gridTokenInfo');
        gridRefresh.innerHTML = '';

        let txRefresh = document.getElementById('transactionList');
        txRefresh.innerHTML = '';

        currencyArray = [];

        latestTxArray = [];

        accountData = [];

        var list = JSON.parse(localStorage.getItem("key"));

        if (list == null) {
            var account = '';
        } else {
            var account = list[0].Active;
        };

        api.on('connected', () => {
            console.log('connected');
        });

        api.on('close', (code) => {
            console.log('disconnected, code:', code)
        });

        api.on('state', () => {
            console.log("The connection state has changed");
        });

        api.ready().then(() => {

            api.send({
                command: "account_info",
                account: account[0],
                strict: "true",
                ledger_index: "current",
                queue: "true"

            }).then((x) => {

                accountData = [
                    x.account_data.urlgravatar,
                    x.account_data.Account,
                    'None',
                    x.account_data.Domain,
                    x.account_data.EmailHash,
                    'None',
                    x.account_data.Sequence,
                    x.account_data.MessageKey
                ];

                var j;
                var temp;

                for (j = 0; j < 1; j++) {

                    temp = document.createElement('img');
                    temp.className = 'accountImage';
                    temp.src = accountData[j];
                    document.getElementsByClassName('accountList')[0].appendChild(temp);
                }

                for (j = 1; j < accountData.length; j++) {

                    temp = document.createElement('li');
                    temp.className = 'accountInfo';
                    temp.innerHTML = accountDataTitle[j] + ' ' + accountData[j];
                    document.getElementsByClassName('accountList')[0].appendChild(temp);
                }

                currencyArray.push({
                    'issuer': '',
                    'balance': x.account_data.Balance / 1000000,
                    'currency': 'XRP'
                });

            });
        });

        api.ready().then(() => {

            api.send({
                command: "account_tx",
                account: account[0],
                ledger_index_min: -1,
                ledger_index_max: -1,
                binary: false,
                limit: 10,
                forward: false
            }).then((x) => {

                var j;
                var temp;
                var type = [];
                var carrier = [];
                var selector = [];

                for (j = 0; j < x.transactions.length; j++) {
                    if (x.transactions[j].tx.Account == accountData[1]) {
                        type.push('Sent');
                        carrier.push('to');
                        selector.push(x.transactions[j].tx.Destination);
                    } else {
                        type.push('Received');
                        carrier.push('from');
                        selector.push(x.transactions[j].tx.Account);
                    }
                }

                for (j = 0; j < x.transactions.length; j++) {
                    temp = document.createElement('li');
                    temp.className = 'tx';
                    temp.innerHTML = type[j] + ' ' + x.transactions[j].tx.Amount / 1000000 + " XRP " + carrier[j] + ' ' + selector[j].substring(0, 7) + "..." + selector[j].slice(selector[j].length - 7);
                    document.getElementsByClassName('transactionList')[0].appendChild(temp);
                }
            });
        });

        api.ready().then(() => {

            api.send({
                command: "account_lines",
                account: account[0],
                ledger_index: "validated"

            }).then((x) => {

                var i;

                for (i = 0; i < x.lines.length; i++) {

                    if (x.lines[i].currency == '534F4C4F00000000000000000000000000000000') {
                        x.lines[i].currency = 'SOLO'
                    }

                    currencyArray.push({
                        'issuer': x.lines[i].account,
                        'balance': x.lines[i].balance,
                        'currency': x.lines[i].currency
                    });

                };

                var j;
                var temp;

                for (j = 0; j < currencyArray.length; j++) {
                    temp = document.createElement('div');
                    temp.className = 'currency';
                    temp.innerHTML = currencyArray[j].currency + '<br></br>' + currencyArray[j].balance;
                    document.getElementsByClassName('gridTokenInfo')[0].appendChild(temp);
                }

            });
        });

        /*api.on('ledger', () => {

              api.request("account_info", {
                account: account,
                strict: "true",
                ledger_index: "current",
                queue: "true"
            }).then((x) => {
                console.log(x);
            });
        });*/
    });

    return ( 
    
    <div>
        <h2 > ACCOUNT </h2>

            <div className = 'grid_wrapper' >

                <div className = 'listAccountInfo' >
                    <ul id = 'accountList'
                    className = 'accountList' >
                    </ul> 
                </div>

                <div id = 'gridTokenInfo'
                className = 'gridTokenInfo'> 
                </div>

                <div className = 'listTransactions' >
                    <ul id = 'transactionList'
                    className = 'transactionList' >
                    </ul> 
                </div> 
            </div> 
        </div>
    );
}

export default Account;