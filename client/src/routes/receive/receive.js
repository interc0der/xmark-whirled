import React from "react";
import './receive.css';
import { useState, useEffect } from 'react';
import image from "../../logo/image.png";

const qr = require("qrcode");

const client = require("xrpl-client");
const api = new client.XrplClient("wss://s.altnet.rippletest.net:51233");

let currencyArray = [];
var list = JSON.parse(localStorage.getItem("key"));

if (list == null) {
    var publickey = '';
} else {
    var publickey = list[0].Active;
};

const getQRcodeImage = async() => {
    try {
        let canvas = await qr.toCanvas(publickey[0], { width: 500 });
        canvas.className = 'innerCanvas';
        //adding a log at center        
        const imgDim = { width: 100, height: 100 }; //logo dimension
        var context = canvas.getContext('2d');

        var imageObj = new Image();

        imageObj.src = image;
        imageObj.onload = function() {
            context.drawImage(imageObj,
                canvas.width / 2 - imgDim.width / 2,
                canvas.height / 2 - imgDim.height / 2,
                imgDim.width,
                imgDim.height);
        }
        return canvas;
    } catch (e) {
        console.error(e);
        return "";
    }
}

function Receive() {

    useEffect(() => {

        currencyArray = [];

        var list = JSON.parse(localStorage.getItem("key"));
        var publickey = list[0].Active;

        getQRcodeImage().then(res => {

            document.getElementsByClassName('canvas')[0].appendChild(res);
        });


        /*qr.toCanvas(document.getElementById('canvas'), publickey[0], {width: 500}, function (error) {
          if (error) 
            console.error(error)
        })*/



        api.ready().then(() => {

            api.send({
                command: "account_info",
                account: publickey[0],
                strict: "true",
                ledger_index: "current",
                queue: "true"

            }).then((x) => {

                console.log(x)

                currencyArray.push({
                    'issuer': '',
                    'balance': x.account_data.Balance / 1000000,
                    'currency': 'XRP'
                });
            })
        })


        api.ready().then(() => {

            var list = JSON.parse(localStorage.getItem("key"));
            var publickey = list[0].Active;

            api.send({
                command: "account_lines",
                account: publickey[0],
                ledger_index: "validated"

            }).then((x) => {

                var i;

                for (i = 0; i < x.lines.length; i++) {

                    if (x.lines[i].currency == '534F4C4F00000000000000000000000000000000') {
                        x.lines[i].currency = 'SOLO'
                    }

                    currencyArray.push({
                        'issuer': x.lines[i].account,
                        'balance': x.lines[i].balance,
                        'currency': x.lines[i].currency
                    });
                };


                var j;
                var temp;

                for (j = 0; j < currencyArray.length; j++) {
                    temp = document.createElement('li');
                    temp.className = 'currency';
                    temp.innerHTML = currencyArray[j].currency + ' ' + currencyArray[j].balance;
                    document.getElementsByClassName('acceptedList')[0].appendChild(temp);
                }
            });
        })
    })

    return ( 
        <div>
            <h2> RECEIVE </h2>

                <div id = "gridparent" >

                    <div id = "gridchild1" >
                        <h2 > Account Address </h2> 
                            <div className = "canvas"></div>      
                            <div id = "pubkey" > { publickey[0] }</div> 
                    </div>

                    <div id = "gridchild2" >
                        <h2 > Receivable Assets </h2> 
                            <div id = 'tokenList'
                            className = 'tokenList' >
                                <ul id = 'acceptedList'
                                className = 'acceptedList' >
                                </ul> 
                            </div> 
                    </div> 
            </div>
        </div>
    );
}


export default Receive;